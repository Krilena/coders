﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CodersLib;

namespace CodersApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<BufferCoder> _coders = new List<BufferCoder>();

        public MainWindow()
        {
            InitializeComponent();

            _coders.Add(new BufferCoder(new HuffmanCoder()));
            _coders.Add(new BufferCoder(new ShannonFanoСoder()));
            _coders.Add(new BufferCoder(new Lz77Coder()));
            _coders.Add(new BufferCoder(new Lz78Coder()));
           // _coders.Add(new BufferCoder(new ArithmeticCoder()));
            _coders.Add(new BufferCoder(new RleAndBurrowsWheelerCoder()));
        }

        private void CodingButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectCodersComboBox.SelectedIndex == -1)
                return;

            var button = sender as Button;

            try
            {
                if (button == getAlphabetButton)
                    _coders[selectCodersComboBox.SelectedIndex].SetAlphabet(textForCodingTextBox.Text);

                if (button == codingButton)
                        _coders[selectCodersComboBox.SelectedIndex].Code(textForCodingTextBox.Text);

                if (button == decodingButton)
                        _coders[selectCodersComboBox.SelectedIndex].Decode(textForCodingTextBox.Text);
            }
            catch (AlphabetNotSetException)
            {
                MessageBox.Show("Актуальный словарь не установлен.");
            }
            catch
            {
                MessageBox.Show("Ошибка работы кодировщика.");
            }

            FillCoderMenu(_coders[selectCodersComboBox.SelectedIndex]);
            typeDiagnosticLabel.Content = (selectCodersComboBox.SelectedValue as Label).Content;
        }

        private void FillCoderMenu(BufferCoder buffer)
        {
            codingTextBox.Text = buffer.CodedText;
            decodingTextBox.Text = buffer.DecodedText;

            dictionaryListBox.Items.Clear();
            foreach (var item in buffer.Alphabet)
                dictionaryListBox.Items.Add(item);
        }
    }
}
