﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodersLib;

namespace CodersApp
{
    class BufferCoder
    {
        private ICoder _coder;

        public string CodedText { get; private set; } = "";
        public string DecodedText { get; private set; } = "";
        public IEnumerable<string> Alphabet { get; private set; } = new List<string>();

        public BufferCoder(ICoder coder)
        {
            _coder = coder;
        }

        public void SetAlphabet(string source)
        {
            _coder.SetAlphabet(source);
            Alphabet = _coder.GetAlphabetToString();
        }

        public void Code(string source)
        {
            CodedText = _coder.Code(source);
        }

        public void Decode(string source)
        {
            DecodedText = _coder.Decode(source);
        }
    }
}
