﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    abstract public class AbstractSymbolicCoders : ICoder
    {
        private Dictionary<char, string> _alphabet = new Dictionary<char, string>();

        protected Dictionary<char, string> Alphabet
        {
            get => _alphabet;
            set
            {
                _alphabet = value;
                _reverseAlphabet = Alphabet.ToDictionary(x => x.Value, x => x.Key);
            }
        }

        protected Dictionary<string, char> _reverseAlphabet { get; private set; } = new Dictionary<string, char>();

        public string Code(string source)
        {
            if (Alphabet.Count==0 && source.Length!=0)
                throw new AlphabetNotSetException("Unable to code because alphabet not set.");

            var sb = new StringBuilder("");

            foreach (var @char in source)
                sb.Append(Alphabet[@char]);

            return sb.ToString();
        }

        public string Decode(string source)
        {
            if (Alphabet == null)
                throw new AlphabetNotSetException("Unable to decode because alphabet not set.");

            var sb = new StringBuilder("");
            var sb2 = new StringBuilder("");
            foreach (var @char in source)
            {
                sb2.Append(@char);
                var code = sb2.ToString();

                if (!_reverseAlphabet.ContainsKey(code))
                    continue;

                sb.Append(_reverseAlphabet[code]);
                sb2 = new StringBuilder("");
            }

            return sb.ToString();
        }

        public IEnumerable<string> GetAlphabetToString()
        {
            return Alphabet.Select(item => String.Format($"[{item.Key} , {item.Value}]"));
        }

        protected IDictionary<char, long> GetFrequencyDictionary(string source)
        {
            var frequencyResponse = new Dictionary<char, long>();

            foreach (var @char in source)
                if (frequencyResponse.ContainsKey(@char))
                    frequencyResponse[@char]++;
                else
                    frequencyResponse.Add(@char, 1);

            return frequencyResponse;
        }

        abstract public void SetAlphabet(string source);
    }

}
