﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    public class HuffmanCoder : AbstractSymbolicCoders
    {
        public override void SetAlphabet(string source)
        {
            var chars = GetFrequencyDictionary(source);
            Alphabet = new Dictionary<char, string>(GetAlphabet(chars));
        }

        private IDictionary<char, string> GetAlphabet(IDictionary<char, long> frequencyResponse)
        {
            if (frequencyResponse.Count == 0)
                return new Dictionary<char, string>();

            var root = GetFrequencyTree(frequencyResponse);
            var queue = new Queue<Node>();
            queue.Enqueue(root);
            var newDictionary = new Dictionary<char, string>();

            while (queue.Any())
            {
                var node = queue.Dequeue();

                if (node.Char != '\0')
                    newDictionary.Add(node.Char, node.Code);
                else
                {
                    node.Left.Code = string.Format($"{node.Code}0");
                    node.Right.Code = string.Format($"{node.Code}1");
                    queue.Enqueue(node.Left);
                    queue.Enqueue(node.Right);
                }
            }

            return newDictionary;
        }

        private Node GetFrequencyTree(IDictionary<char, long> requencyDictionary)
        {
            var list = requencyDictionary.Select(item => new Node { Char = item.Key, Frequency = item.Value }).ToList();

            while (list.Count > 1)
            {
                list.Sort((x, y) => y.Frequency.CompareTo(x.Frequency));
                var left = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                var right = list[list.Count - 1];
                var node = new Node { Left = left, Right = right, Frequency = left.Frequency + right.Frequency };
                list[list.Count - 1] = node;
            }

            return list[0];
        }

        private class Node
        {
            public Node Left;
            public Node Right;
            public char Char;
            public long Frequency;
            public string Code = "";
        }
    }

}
