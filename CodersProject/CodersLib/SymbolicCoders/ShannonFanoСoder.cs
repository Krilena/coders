﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    public class ShannonFanoСoder : AbstractSymbolicCoders
    {
        public override void SetAlphabet(string source)
        {
            Alphabet = new Dictionary<char, string>(GetAlphabet(GetFrequencyDictionary(source)));
        }

        private IDictionary<char, string> GetAlphabet(IDictionary<char, long> frequencyDictionary)
        {
            if (frequencyDictionary.Count == 0)
                return new Dictionary<char, string>();

            var alphabet = new Dictionary<char, string>();
            var root = new Node
            {
                CharsAndFrequency = new Queue<KeyValuePair<char, long>>(frequencyDictionary.OrderByDescending(x => x.Value).AsEnumerable()),
                AllFrequency = frequencyDictionary.Sum(x => x.Value)
            };

            var nodes = new Queue<Node>();
            nodes.Enqueue(root);

            while (nodes.Count != 0)
            {
                foreach (var item in GetChildrenOfNode(nodes.Dequeue()))
                    if (item.CharsAndFrequency.Count >= 2)
                        nodes.Enqueue(item);
                    else
                        if (item.CharsAndFrequency.Count == 1)
                        alphabet.Add(item.CharsAndFrequency.Dequeue().Key, item.Code);
            }

            return alphabet;
        }

        private Node[] GetChildrenOfNode(Node node)
        {
            var newNodes = new Node[]
                {
                    new Node(),
                    new Node()
                };

            while (node.CharsAndFrequency.Count != 0)
            {
                var pair = node.CharsAndFrequency.Dequeue();
                if (newNodes[1].AllFrequency + pair.Value < newNodes[0].AllFrequency + pair.Value)
                {
                    newNodes[1].CharsAndFrequency.Enqueue(pair);
                    newNodes[1].AllFrequency += pair.Value;
                }
                else
                {
                    newNodes[0].CharsAndFrequency.Enqueue(pair);
                    newNodes[0].AllFrequency += pair.Value;
                }
            }

            newNodes[0].Code = string.Format($"{node.Code}0");
            newNodes[1].Code = string.Format($"{node.Code}1");

            return newNodes;
        }

        private class Node
        {
            public Queue<KeyValuePair<char, long>> CharsAndFrequency = new Queue<KeyValuePair<char, long>>();
            public long AllFrequency;
            public string Code = "";
        }
    }

}
