﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    public class AlphabetNotSetException : Exception
    {
        public AlphabetNotSetException(string message) : base(message)
        {
        }
    }
}
