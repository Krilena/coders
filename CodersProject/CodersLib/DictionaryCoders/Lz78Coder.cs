﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    public class Lz78Coder : ICoder
    {
        readonly int _maxSizeDictionary;
        public Dictionary<int, string> Alphabet { get; private set; } = new Dictionary<int, string>();
        public List<(int,char)> Marks { get; private set; }

        public Lz78Coder(int maxSizeDictionary = 1000) 
        {
            _maxSizeDictionary = maxSizeDictionary;
        }

        public string Code(string source)
        {
            return string.Join(",", Marks.Select(x => $"{x.Item1},{(int)x.Item2}").ToArray());
        }

        public string Decode(string source)
        {
            var array = source.Split(',');
            var alphabet = new Dictionary<int,string>();
            var @string = new StringBuilder();
            var index = 1;

            for (var i = 0; i < array.Length; i += 2)
            {
                var (position, @char) = (int.Parse(array[i]), (char)int.Parse(array[i + 1]));
                var substring = new StringBuilder();

                if (position != 0)
                    substring.Append(alphabet[position]);

                substring.Append(@char);
                @string.Append(substring);
                alphabet.Add(index, substring.ToString());
                index++;

                if (alphabet.Count >= _maxSizeDictionary)
                { 
                    alphabet.Clear();
                    index = 1;
                }
            }

            return @string.ToString();
        }

        public IEnumerable<string> GetAlphabetToString()
        {
            return Alphabet.Select(x => string.Format($"({x.Key},{x.Value})"));
        }

        public void SetAlphabet(string source)
        {
            var sb = new StringBuilder();
            var alphabet = new Dictionary<string, int>();
            var marks = new List<(int, char)>();
            var index = 1;
            var position = 0;

            foreach (var @char in source)
            {
                sb.Append(@char);
                var @string = sb.ToString();

                if (alphabet.ContainsKey(@string))
                    position = alphabet[@string];
                else
                {
                    alphabet.Add(@string, index);
                    index++;
                    marks.Add((position, sb[sb.Length - 1]));
                    position = 0;
                    sb.Clear();

                    if (alphabet.Count >= _maxSizeDictionary)
                    {
                        alphabet.Clear();
                        index = 1;
                    }
                }  
            }

            Marks = marks;
            Alphabet = alphabet.ToDictionary(x => x.Value, x => x.Key.ToString());
        }
    }
}
