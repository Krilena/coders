﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersLib
{
    public interface ICoder
    {
        void SetAlphabet(string source);
        IEnumerable<string> GetAlphabetToString();
        string Code(string source);
        string Decode(string source);
    }
}
