﻿using System.Collections.Generic;

namespace CodersLib
{
    public class RleAndBurrowsWheelerCoder: ICoder
    {
        public  string Code(string str)
        {
            var shifts = new List<string>(RleAndBurrowsWheeler.Bw.GetShiftsOf(str));
            shifts.Sort();
            var position = RleAndBurrowsWheeler.Bw.GetIndexOf(str, shifts);

            return RleAndBurrowsWheeler.Rle.Transform(shifts, position).ToString();
        }

        public string Decode(string source)
        {
            return "";
        }

        public IEnumerable<string> GetAlphabetToString()
        {
            return new List<string>();
        }

        public void SetAlphabet(string source)
        {
        }
    }
}